//
//  CategoriesSM.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

struct CategoriesSM: Codable, Equatable {
    let categories: [Category]?
    
    enum CodingKeys: String, CodingKey {
        case categories = "drinks"
    }
}

struct Category: Codable, Equatable {
    let strCategory: String
}
