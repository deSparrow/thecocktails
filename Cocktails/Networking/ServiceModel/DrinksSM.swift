//
//  DrinksSM.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

struct DrinksSM: Codable, Equatable {
    let drinks: [Drink]?
}

struct Drink: Codable, Equatable {
    let strDrink: String
    let strDrinkThumb: String?
    let idDrink: String
}
