//
//  DrinksServiceManager.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

typealias DrinksCompletionHandler = (Result<DrinksSM, GeneralError>)

protocol DrinksRepository: AnyObject {
    func getDrinks(for category: String, completion: @escaping (DrinksCompletionHandler) -> Void)
}

class DrinksServiceManager: DrinksRepository {
    private func getDrinksEndpoint(category: String) -> String {
        return "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=\(category)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    }
    private let decoder = JSONDecoder()
    
    public func getDrinks(for category: String, completion: @escaping (DrinksCompletionHandler) -> Void) {
        guard let url = URL(string: getDrinksEndpoint(category: category)) else { return }
        URLSession.shared.dataTask(with: url) { result in
            switch result {
            case .failure(let error):
                if let error = error as? GeneralError {
                    completion(.failure(error))
                } else {
                    completion(.failure(GeneralError.unknownError))
                }
            case .success(let (_, data)):
                if let drinks = try? self.decoder.decode(DrinksSM.self, from: data) {
                    completion(.success(drinks))
                } else {
                    completion(.failure(.error(msg: "Error while fetching drinks!")))
                }
            }
        }.resume()
    }
}
