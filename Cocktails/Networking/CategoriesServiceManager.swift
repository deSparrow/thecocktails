//
//  CategoriesServiceManager.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

typealias CategoriesCompletionHandler = (Result<CategoriesSM, GeneralError>)

protocol CategoriesRepository: AnyObject {
    func getCategories(completion: @escaping (CategoriesCompletionHandler) -> Void)
}

class CategoriesServiceManager: CategoriesRepository {
    private func getCategoriesEndpoint(c: String) -> String {
      return "https://www.thecocktaildb.com/api/json/v1/1/list.php?c=\(c)"
    }
    private let decoder = JSONDecoder()
    
    public func getCategories(completion: @escaping (CategoriesCompletionHandler) -> Void) {
        guard let url = URL(string: getCategoriesEndpoint(c: "list")) else { return }
        URLSession.shared.dataTask(with: url) { result in
            switch result {
            case .failure(let error):
                if let error = error as? GeneralError {
                    completion(.failure(error))
                } else {
                    completion(.failure(GeneralError.unknownError))
                }
            case .success(let (_, data)):
                if let categories = try? self.decoder.decode(CategoriesSM.self, from: data) {
                    completion(.success(categories))
                } else {
                    completion(.failure(.error(msg: "Error while fetching categories!")))
                }
            }
        }.resume()
    }
}
