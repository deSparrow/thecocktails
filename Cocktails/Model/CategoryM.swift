//
//  CategoriesM.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

class CategoryM {
    let categoryName: String
    
    init(from category: Category) {
        categoryName = category.strCategory
    }
}
