//
//  GeneralError.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

enum GeneralError: Error {
    case error(msg: String)
    case unknownError
}
