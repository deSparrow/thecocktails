//
//  ImageLoader.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation
import UIKit

typealias ImageCacheLoaderCompletionHandler = ((UIImage) -> ())

class ImageLoader {
    
    private let imageCache = NSCache<NSString, UIImage>()
    
    func load(from url: URL?, completionHandler: @escaping ImageCacheLoaderCompletionHandler) {
        guard let url = url else {
            return
        }
        
        guard let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) else {
            DispatchQueue.global().async { [weak self] in
                if let this = self, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        this.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                        completionHandler(image)
                    }
                }
            }
            return
        }
        
        DispatchQueue.main.async {
            completionHandler(cachedImage)
        }
    }
}
