//
//  DrinksM.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

class DrinkM {
    let name: String
    let id: Int
    private(set) var imageUrl: URL?
    
    init(from drink: Drink) {
        name = drink.strDrink
        if let imageUrl = drink.strDrinkThumb, let url = URL(string: imageUrl) {
            self.imageUrl = url
        }
        id = Int(drink.idDrink) ?? -1
    }
}
