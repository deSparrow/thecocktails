//
//  DrinksViewController.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import UIKit

class DrinksViewController: UITableViewController {
    
    private var presenter: DrinksPresenter!
    private let imageLoader = ImageLoader()
    private let alternativeCellImage = UIImage(named: "AltThumbnail")

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = DrinksPresenter(view: self, drinksRepository: DrinksServiceManager(), categoriesRepository: CategoriesServiceManager())
        presenter.viewLoaded()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.drinks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Drinks")
        let drink = presenter.drinks[indexPath.row]
        cell.textLabel?.text = drink.name
        cell.imageView?.image = alternativeCellImage
        imageLoader.load(from: drink.imageUrl) { image in
            if let cell = tableView.cellForRow(at: indexPath) {
                cell.imageView?.image = image
            }
        }
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Filter", let categoriesVC = segue.destination as? CategoriesViewController {
            categoriesVC.presenter.selectedCategory = presenter.selectedCategory
            categoriesVC.categoriesDelegate = self
        }
    }

}

extension DrinksViewController: DrinksView {
    func refreshDrinks() {
        tableView.reloadData()
    }
    
    func displayErrorAlert(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        present(alert, animated: true)
    }
}

extension DrinksViewController: CategoriesDelegate {
    func didChangeCategory(category: CategoryM) {
        presenter.didChangeCategory(category)
    }
    
}
