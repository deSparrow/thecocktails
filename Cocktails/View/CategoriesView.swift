//
//  CategoriesView.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

protocol CategoriesView: AnyObject {
    func refreshCategories()
    func displayErrorAlert(_ title: String, _ message: String)
}
