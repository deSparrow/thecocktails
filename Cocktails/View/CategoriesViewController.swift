//
//  CategoriesViewController.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import UIKit

protocol CategoriesDelegate: AnyObject {
    func didChangeCategory(category: CategoryM)
}

class CategoriesViewController: UITableViewController {
    
    var presenter: CategoriesPresenter = CategoriesPresenter(categoriesRepository: CategoriesServiceManager())
    weak var categoriesDelegate: CategoriesDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.injectView(view: self)
        presenter.viewLoaded()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Categories")
        let category = presenter.categories[indexPath.row]
        cell.textLabel?.text = category.categoryName
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = presenter.categories[indexPath.row]
        categoriesDelegate?.didChangeCategory(category: category)
        navigationController?.popToRootViewController(animated: true)
    }
}

extension CategoriesViewController: CategoriesView {
    func refreshCategories() {
        tableView.reloadData()
        guard let index = presenter.getIndexOfSelectedCategory() else {
            return
        }
        let indexPath = IndexPath(row: index, section: 0)
        tableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)
    }
    
    func displayErrorAlert(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        present(alert, animated: true)
    }
}
