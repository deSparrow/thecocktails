//
//  CategoriesPresenter.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

class CategoriesPresenter {
    weak private var view: CategoriesView?
    private let categoriesRepository: CategoriesRepository
    var categories: [CategoryM] = []
    var selectedCategory: CategoryM?
    
    init(categoriesRepository: CategoriesRepository) {
        self.categoriesRepository = categoriesRepository
    }
    
    func injectView(view: CategoriesView) {
        self.view = view
    }
    
    func viewLoaded() {
        fetchCategories()
    }
    
    func getIndexOfSelectedCategory() -> Int? {
        return categories.firstIndex(where: { category -> Bool in
            category.categoryName == selectedCategory?.categoryName
        })
    }
    
    private func fetchCategories() {
        categoriesRepository.getCategories { [weak self] completion in
            guard let this = self else { return }
            switch completion {
            case .failure(let error):
                var errorMsg = "Unknown error!"
                if case .error(let msg) = error {
                    errorMsg = msg
                }
                this.categories = []
                DispatchQueue.main.async {
                    this.view?.displayErrorAlert("Error", errorMsg)
                }
            case .success(let categories):
                guard let categories = categories.categories else {
                    this.view?.displayErrorAlert("Error", "There are no categories!")
                    return
                }
                this.categories = categories.map { CategoryM(from: $0) }
            }
            DispatchQueue.main.async {
                this.view?.refreshCategories()
            }
        }
    }
    
}
