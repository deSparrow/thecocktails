//
//  DrinksPresenter.swift
//  Cocktails
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import Foundation

class DrinksPresenter {
    weak private var view: DrinksView?
    private let drinksRepository: DrinksRepository
    private let categoriesRepository: CategoriesRepository
    private(set) var drinks: [DrinkM] = []
    private(set) var selectedCategory: CategoryM?
    private(set) var categories: [CategoryM]? {
        didSet {
            if let categoryName = categories?.first?.categoryName {
                selectedCategory = categories?.first
                fetchDrinks(category: categoryName)
            }
        }
    }
    
    init(view: DrinksView, drinksRepository: DrinksRepository, categoriesRepository: CategoriesRepository) {
        self.view = view
        self.drinksRepository = drinksRepository
        self.categoriesRepository = categoriesRepository
    }
    
    func viewLoaded() {
        fetchCategories()
    }
    
    func didChangeCategory(_ category: CategoryM) {
        selectedCategory = category
        fetchDrinks(category: category.categoryName)
    }
    
    private func fetchCategories() {
        categoriesRepository.getCategories { [weak self] completion in
            guard let this = self else { return }
            switch completion {
            case .failure(let error):
                var errorMsg = "Unknown error!"
                if case .error(let msg) = error {
                    errorMsg = msg
                }
                DispatchQueue.main.async {
                    this.view?.displayErrorAlert("Error", errorMsg)
                }
            case .success(let categories):
                guard let categories = categories.categories else {
                    this.view?.displayErrorAlert("Error", "There are no categories!")
                    return
                }
                this.categories = categories.map { CategoryM(from: $0) }
            }
        }
    }
    
    private func fetchDrinks(category: String) {
        drinksRepository.getDrinks(for: category) { [weak self] completion in
            guard let this = self else { return }
            switch completion {
            case .failure(let error):
                var errorMsg = "Unknown error!"
                if case .error(let msg) = error {
                    errorMsg = msg
                }
                this.drinks = []
                DispatchQueue.main.async {
                    this.view?.displayErrorAlert("Error", errorMsg)
                }
            case .success(let drinks):
                guard let drinks = drinks.drinks else {
                    this.view?.displayErrorAlert("Error", "There are no drinks!")
                    return
                }
                this.drinks = drinks.map { DrinkM(from: $0) }
            }
            DispatchQueue.main.async {
                this.view?.refreshDrinks()
            }
        }
    }
    
}
