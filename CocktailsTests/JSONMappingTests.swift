//
//  JSONMappingTests.swift
//  CocktailsTests
//
//  Created by Fabe on 07/10/2019.
//  Copyright © 2019 Fabian Olszewski. All rights reserved.
//

import XCTest
@testable import Cocktails

class JSONMappingTests: XCTestCase {

    func testDrinksServiceModelJSONConversion() {
        let jsonData = Data("""
            {
                "drinks": [
                  {
                    "strDrink": "Afternoon",
                    "strDrinkThumb": "https://www.thecocktaildb.com/images/media/drink/vyrurp1472667777.jpg",
                    "idDrink": "13162"
                  },
                  {
                    "strDrink": "Cafe Savoy",
                    "strDrinkThumb": "https://www.thecocktaildb.com/images/media/drink/vqwptt1441247711.jpg",
                    "idDrink": "14181"
                  }
                ]
            }
        """.utf8)
        let drinksSM = try? JSONDecoder().decode(DrinksSM.self, from: jsonData)
        
        XCTAssertNotNil(drinksSM)
        
        let drinksSM2 = DrinksSM(drinks: [Drink(strDrink: "Afternoon", strDrinkThumb: "https://www.thecocktaildb.com/images/media/drink/vyrurp1472667777.jpg", idDrink: "13162"), Drink(strDrink: "Cafe Savoy", strDrinkThumb: "https://www.thecocktaildb.com/images/media/drink/vqwptt1441247711.jpg", idDrink: "14181")])
        XCTAssertEqual(drinksSM!, drinksSM2)
    }

    func testCategoriesServiceModelJSONConversion() {
        let jsonData = Data("""
            {
                "drinks": [
                  {
                    "strCategory": "Ordinary Drink"
                  },
                  {
                    "strCategory": "Cocktail"
                  }
                ]
            }
        """.utf8)
        let categoriesSM = try? JSONDecoder().decode(CategoriesSM.self, from: jsonData)
        
        XCTAssertNotNil(categoriesSM)
        
        let categoriesSM2 = CategoriesSM(categories: [Category(strCategory: "Ordinary Drink"), Category(strCategory: "Cocktail")])
        XCTAssertEqual(categoriesSM!, categoriesSM2)
    }
    
}
